//
//  DesafioiOSConcreteTests.swift
//  DesafioiOSConcreteTests
//
//  Created by ECX Card on 24/01/18.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import XCTest
@testable import DesafioiOSConcrete

class DesafioiOSConcreteTests: XCTestCase {
    
    var RepositoryListVC : RepositoryListViewController?
    var PullRequestVC: PullRequestsViewController?
    var repoList: [Repository] = []

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        RepositoryListVC = Helper.storyBoardWithName(name: "Main", storyboardId: "StoryboardRepositoryList") as? RepositoryListViewController
        PullRequestVC = Helper.storyBoardWithName(name: "Main", storyboardId: "StoryboardPullRequest") as? PullRequestsViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    var waitExpectation: XCTestExpectation?
    
    ///Testa a requisição de repositorios da API do GitHub, a primeira página terá 30 itens
    func testRepositoryRequestList() {
        if let RepList = RepositoryListVC{
            waitExpectation = expectation(description: "Expectations from \(#function)")
            _ = RepList.view
            
            RepList.page = 1
            RepList.requestRepositories()
            
            //Espera a API retornar
            Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(waitFullfill), userInfo: nil, repeats: false)
            
            //Se esperar mais de 6 segundos termina o teste
            waitForExpectations(timeout: 6, handler: nil)
    
            XCTAssertEqual(RepList.repositoryList.count, 30)
        }
    }
    
    ///Testa a requisição de pull request do repositório selecionado no método
    func testPullRequestList(){
        if let PullReqVC = PullRequestVC{
            waitExpectation = expectation(description: "Expectations from \(#function)")
            _ = PullReqVC.view
            
            //User selecionado para teste
            let user = GitUser(username: "octocat", photo: "")
            PullReqVC.repositorySelected = Repository(name: "Hello-World", description: "Teste XC", forksCount: 5, starsCount: 5, gitOwner: user)
            PullReqVC.requestPullRequestList()
            
            //Espera a API retornar
            Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(waitFullfill), userInfo: nil, repeats: false)
            
            //Se esperar mais de 6 segundos termina o teste
            waitForExpectations(timeout: 6, handler: nil)
            
            XCTAssert(PullReqVC.pullRequestList.count > 0)
        }
    }
    
    func waitFullfill(){
        waitExpectation?.fulfill()
    }
    
}

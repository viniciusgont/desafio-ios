//
//  Repository.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import Foundation
import ObjectMapper

enum RepositoryJSONKeys {
    static var repositoryName = "name"
    static var repositoryDescription = "description"
    static var repositoryForks = "forks_count"
    static var repositoryStars = "stargazers_count"
}

enum UserJSONKeys {
    static var userName = "owner.login"
    static var userAvatarURL = "owner.avatar_url"
}

struct GitUser {
    var username: String?
    var photo: String?
    
    var photoURLDescription: URL?{
        if let photo = photo{
            return URL(string: photo)
        }
        return nil
    }
}

class Repository: Mappable {
    
    //Properties
    var name: String?
    var description: String?
    var forksCount: Int?
    var starsCount: Int?
    var gitOwner = GitUser()
    
    init(name: String, description: String, forksCount: Int, starsCount: Int, gitOwner: GitUser) {
        self.name = name
        self.description = description
        self.forksCount = forksCount
        self.starsCount = starsCount
        self.gitOwner = gitOwner
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map[RepositoryJSONKeys.repositoryName]
        description <- map[RepositoryJSONKeys.repositoryDescription]
        forksCount <- map[RepositoryJSONKeys.repositoryForks]
        starsCount <- map[RepositoryJSONKeys.repositoryStars]
        
        gitOwner.username <- map[UserJSONKeys.userName]
        gitOwner.photo <- map[UserJSONKeys.userAvatarURL]
    }
    
    //Details https://api.github.com/repos/login:ReactiveX/repName:learnrx/pulls
}






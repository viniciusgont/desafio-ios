//
//  PullRequest.swift
//  DesafioiOSConcrete
//
//  Created by ECX Card on 23/01/18.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import Foundation
import ObjectMapper

enum PullRequestsJSONKeys {
    static var pullRequestTitle = "title"
    static var pullRequestDescription = "body"
    static var pullRequestDate = "created_at"
    static var pullRequestUrl = "html_url"
}

enum PRAuthorJSONKeys {
    static var userName = "user.login"
    static var userAvatarURL = "user.avatar_url"
}

class PullRequest: Mappable {
    
    //Properties
    var name: String?
    var description: String?
    var date: Date?
    var urlPage: String?
    
    var gitUser = GitUser()
    
    init() {}
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map[PullRequestsJSONKeys.pullRequestTitle]
        description <- map[PullRequestsJSONKeys.pullRequestDescription]
        urlPage <- map[PullRequestsJSONKeys.pullRequestUrl]
        
        if let date = map[PullRequestsJSONKeys.pullRequestDate].currentValue as? String{
            self.date = Helper.getDateWithString(dateString: date)
        }
        
        gitUser.username <- map[PRAuthorJSONKeys.userName]
        gitUser.photo <- map[PRAuthorJSONKeys.userAvatarURL]
    }
    
    
}


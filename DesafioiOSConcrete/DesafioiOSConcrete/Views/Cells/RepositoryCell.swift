//
//  RepositoryCell.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit

class RepositoryCell: BaseTableViewCell {

    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var repositoryForksCount: UILabel!
    @IBOutlet weak var repositoryStarCount: UILabel!
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userPhoto.cornerRadius()
    }
    
    ///Recebe os dados do repository e carregas esses dados na célula da tableview
    func populateWithRepository(repository: Repository){
        
        let errorDescription = "Não encontrado"
        
        if let repName = repository.name{
            repositoryName.text = repName
        } else {
            repositoryName.text = errorDescription
        }
        
        if let repDescription = repository.description{
            repositoryDescription.text = repDescription
        } else {
            repositoryDescription.text = errorDescription
        }
        
        if let repForks = repository.forksCount{
            repositoryForksCount.text = String(repForks)
        } else {
            repositoryForksCount.text = "0"
        }
        
        if let repStars = repository.starsCount{
            repositoryStarCount.text = String(repStars)
        } else {
            repositoryStarCount.text = "0"
        }
        
        if let userAvatar = repository.gitOwner.photoURLDescription{
            userPhoto.sd_setShowActivityIndicatorView(true)
            userPhoto.sd_setIndicatorStyle(.gray)
            userPhoto.sd_setImage(with: userAvatar, completed: nil)
        } 
        
        if let repUsername = repository.gitOwner.username{
            username.text = repUsername
        } else{
            username.text = errorDescription
        }
    }
}

//
//  PullRequestCell.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 22/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit

class PullRequestCell: BaseTableViewCell {

    @IBOutlet weak var pullReqTitle: UILabel!
    @IBOutlet weak var pullReqBody: UILabel!
    @IBOutlet weak var pullReqDate: UILabel!
    
    @IBOutlet weak var userphoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    ///Recebe os dados do repository e carregas esses dados na célula da tableview
    func populateWithPullRequest(pullRequest: PullRequest){
        
        if let pullRequestTitle = pullRequest.name{
            pullReqTitle.text = pullRequestTitle
        } else {
            pullReqTitle.text = errorDescription
        }
        
        if let pullRequestBody = pullRequest.description{
            pullReqBody.text = pullRequestBody
        } else {
            pullReqBody.text = errorDescription
        }
        
        if let pullRequestDate = pullRequest.date{
            pullReqDate.text = Helper.DateToString(date: pullRequestDate)
        } else {
            pullReqDate.text = errorDescription
        }
        
        if let userAvatar = pullRequest.gitUser.photoURLDescription{
            userphoto.sd_setShowActivityIndicatorView(true)
            userphoto.sd_setIndicatorStyle(.gray)
            userphoto.sd_setImage(with: userAvatar, completed: nil)
        }
        
        if let userLogin = pullRequest.gitUser.username{
            username.text = userLogin
        } else {
            username.text = errorDescription
        }
    }
}

//
//  PullRequestsViewController.swift
//  DesafioiOSConcrete
//
//  Created by ECX Card on 23/01/18.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//
import UIKit
import Alamofire
import SDWebImage

//MARK: PullRequestsViewController
class PullRequestsViewController: BaseViewController {
    
    var pullRequestList: [PullRequest] = []
    var repositorySelected: Repository?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = repositorySelected?.name
        
        prepareTableView()
        requestPullRequestList()
    }
    
    func prepareTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 140.0
                
        tableView.register(UINib(nibName: "PullRequestCell", bundle: nil), forCellReuseIdentifier: "PullRequestCell")
        tableView.tableFooterView = UIView()
    }
    
    var getPullRequestList: Request?
    
    ///Faz a requisicao dos dados na API e mostra na tela
    func requestPullRequestList(){
        getPullRequestList?.cancel()
        Helper.hudShowWithStatus(status: "Carregando...")
        
        guard let repositorySelected = repositorySelected else {
            Helper.hudDismiss()
            Helper.showAlert(viewController: self, title: "Erro", message: "Erro ao transmitir os dados")
            return
        }
        
        //Details example https://api.github.com/repos/login:ReactiveX/repName:learnrx/pulls
        //Requisita a API via Alamofire(router), se success o closure é acionado
        getPullRequestList = APIClient.getPullRequest(owner: repositorySelected.gitOwner.username!, repositoryName: repositorySelected.name!, success: {[weak self] (pullRequests) in
            Helper.hudDismiss()
            
            if let strongSelf = self{
                
                if pullRequests.count > 0{
                    strongSelf.pullRequestList.append(contentsOf: pullRequests)
                    strongSelf.tableView.reloadData()
                }else{
                    Helper.showAlert(viewController: strongSelf, title: "Repositório sem pull requests", message: "Busca não retornou resultados")
                }
            }
            
        }) { (error) in
            print(error)
            Helper.hudDismiss()
            Helper.showAlert(viewController: self , title: "Erro", message: error.description)
        }
    }
    
    deinit {
        getPullRequestList?.cancel()
    }
}

//MARK: Tableviews
extension PullRequestsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as? PullRequestCell else{
            fatalError("Célula não encontrada na RepositoryCell")
        }
        
        cell.populateWithPullRequest(pullRequest: (pullRequestList[indexPath.row]))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let htmlURL = self.pullRequestList[indexPath.row].urlPage{
            UIApplication.shared.openURL(URL(string: htmlURL)!)
        }
    }
    
}

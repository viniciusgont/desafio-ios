//
//  RepositoryListViewController.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class RepositoryListViewController: BaseViewController {
    
    var repositoryList: [Repository] = []
    var page: Int = 1
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareTableView()
        requestRepositories()
    }
    
    func prepareTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 90.0
        
        tableView.register(UINib(nibName: "RepositoryCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
        tableView.tableFooterView = UIView()
    }
    
    var getRepositoriesRequest: Request?
    
    ///Faz a requisicao dos dados na API e mostra na tela
    func requestRepositories(){
        getRepositoriesRequest?.cancel()
        
        Helper.hudShowWithStatus(status: "Carregando...")
        
        //Requisita a API via Alamofire(router), se success o closure é acionado
        getRepositoriesRequest = APIClient.getRepositories(type: "stars", page: page, success: {[weak self] (repositories) in
            Helper.hudDismiss()

            if let strongSelf = self{
                strongSelf.repositoryList.append(contentsOf: repositories)
                strongSelf.tableView.reloadData()
            }
            
        }) { (error) in
            print(error)
            Helper.hudDismiss()
            Helper.showAlert(viewController: self , title: "Erro", message: error.description)
        }
    }
    
    deinit {
        getRepositoriesRequest?.cancel()
    }
}

//MARK: Tableviews
extension RepositoryListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as? RepositoryCell else{
            fatalError("Célula não encontrada na RepositoryCell")
        }
        
        cell.populateWithRepository(repository: repositoryList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let last = self.repositoryList.count - 1
        //TODO: Qual a page final?
        if indexPath.row == last {
            self.page += 1
            self.requestRepositories()
        }
    }
    
//MARK: Segues
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueToPullRequest", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToPullRequest"{
            let destinationVC = segue.destination as! PullRequestsViewController
            destinationVC.repositorySelected = self.repositoryList[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
}



//
//  BaseViewController.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configActions()
        configAppearence()
    }
    
    func configActions(){
        //override this
    }
    
    func configAppearence(){
        //override this
    }
}

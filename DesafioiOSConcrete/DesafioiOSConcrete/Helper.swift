//
//  Helper.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit
import SVProgressHUD

class Helper {
 
//MARK: General
    
    ///Utilize para facilitar a instancia de ViewControllers do Storyboard
    class func storyBoardWithName(name:String, storyboardId:String? = nil) -> UIViewController{
        if let storyboardId = storyboardId{
            return UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: storyboardId)
        }else{
            return UIStoryboard(name: name, bundle: nil).instantiateInitialViewController()!
        }
    }
    
    ///Utilize para converter uma data de string de formato yyyy-MM-dd'T'HH:mm:ssZ para Date
    class func getDateWithString(dateString:String) -> Date? {
        let fomatter = DateFormatter()
        fomatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = fomatter.date(from: dateString)
        return date
    }
    
    ///Utilize para converter uma data para o formarto dd/mm/yyyy
    class func DateToString(date:Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: date)
    }
}

//MARK: Alert views
extension Helper{
    
    ///Utilize para mostrar um alertviewcontroller com opção
    class func showAlert(viewController: UIViewController, title: String, message: String, block: (() -> Void)? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let retryAction = UIAlertAction(title: "OK", style: .default, handler: { (_) -> Void in
            block?()
        })
        alertController.addAction(retryAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}

//MARK: SVProgressHUD
extension Helper{
    
    ///Mostra o progresso do SVProgress com o status definido
    class func hudShowWithStatus(status: String){
        SVProgressHUD.show(withStatus: status)
    }
    
    ///Dispensa o SVProgressHUD
    class func hudDismiss(){
        SVProgressHUD.dismiss()
    }
}

//MARK: ImageView
extension UIImageView{
    
    ///Coloca borda nos cantos das imagens
    func cornerRadius() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

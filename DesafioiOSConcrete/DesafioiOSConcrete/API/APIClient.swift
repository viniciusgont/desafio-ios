//
//  APIClient.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum Router: URLRequestConvertible{
    static let baseURLAPI = "https://api.github.com"
    
    case SearchJavaFavRepositories(sortType:String, page:Int)
    case RepositoryPullRequestDetails(owner: String, repositoryName: String)
    
    ///Define o metodo e url para a chamada
    var path: (method: HTTPMethod, url: String){
        switch self {
        case .SearchJavaFavRepositories:
            return (.get, "/search/repositories")
        case .RepositoryPullRequestDetails(let owner, let repositoryName):
            return (.get, "/repos/\(owner)/\(repositoryName)/pulls")
        }
    }
    
    ///Constroi a URLRequest para cada chamada na API
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLAPI)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path.url))
        urlRequest.httpMethod = path.method.rawValue
        
        switch self {
        case .SearchJavaFavRepositories(let sortType, let pageNumber):
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["q": "language:Java", "sort" : sortType, "page": pageNumber])
        
        case .RepositoryPullRequestDetails:
            return try Alamofire.JSONEncoding.default.encode(urlRequest)
        }
    }
}

class APIClient {
    static let domain = "com.github.api"
    
    enum errorCode: Int {
        case nonRetryCode = 0
        case retryCode = 1
    }
    
    //MARK: Requests from Controllers
    class func getRepositories(type: String, page: Int,
                               success: @escaping ([Repository]) -> Void,
                               failure: @escaping (NSError) -> Void) -> Request{
    
        return request(urlRequestConvertible: Router.SearchJavaFavRepositories(sortType: type, page: page), JSONKey: "items", success: success, failure: failure)
    
    }
    
    class func getPullRequest(owner: String, repositoryName: String,
                               success: @escaping ([PullRequest]) -> Void,
                               failure: @escaping (NSError) -> Void) -> Request{
        
        return request(urlRequestConvertible: Router.RepositoryPullRequestDetails(owner: owner, repositoryName: repositoryName), JSONKey: "", success: success, failure: failure)
    }
}

//MARK: Request helpers
extension APIClient{
    
    ///Resultado sem array
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible,JSONKey: String,
                                           success:@escaping (T) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return self.request(urlRequestConvertible: urlRequestConvertible, type: T.self, JSONKey: JSONKey,
                            success: { (any) -> Void in
            if let object = any as? T{
                success(object)
            }else{
                failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(any)"]))
            }
        },
        failure: failure)
    }
    
    ///Resultado com array
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible, JSONKey: String,
                                           success:@escaping ([T]) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return self.request(urlRequestConvertible: urlRequestConvertible, isArray: true, type: T.self, JSONKey: JSONKey, success: { (any) -> Void in
            if let objects = any as? [T]{
                success(objects)
            }else{
                failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(any)"]))
            }
        }, failure: failure)
    }
    
    ///Utilizando para disparar a chamada, deixar o resultado mappable e manipulável
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible, isArray:Bool = false, type:T.Type, JSONKey: String,
                                           success:@escaping (Any) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return Alamofire.request(urlRequestConvertible)
            .validate()
            .responseJSON(options: JSONSerialization.ReadingOptions.mutableContainers) { (response) -> Void in
                
                debugPrint(response)
                
                switch response.result {
                case .success(let resultJSON):
                    
                    let mappableObject:Any?
                    if isArray{
                        if !JSONKey.isEmpty{
                            //Procura pela chave definida na API
                            let data = resultJSON as! NSDictionary
                            mappableObject = Mapper<T>().mapArray(JSONArray: data[JSONKey] as! [[String : Any]])
                        } else{
                            mappableObject = Mapper<T>().mapArray(JSONObject: resultJSON)
                        }
                    }else{
                        mappableObject = Mapper<T>().map(JSONObject: resultJSON)
                    }
                    
                    if let mappableObject = mappableObject{
                        success(mappableObject)
                    }else{
                        if response.response != nil{
                            failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(resultJSON)"]))
                        }
                    }
                    
                case .failure(let error):
                    if let alamoResponse = response.response{
                        if alamoResponse.statusCode != 404{
                            failure(NSError(domain: error._domain, code: alamoResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: error.localizedDescription]))
                        }
                    } else {
                        failure(error as NSError)
                    }
                }
        }
    }
}
